var myChart;
var myTable;
var csrftoken;
var groupby = 'country';
var start_date;
var end_date;

var dataTable,
    domTable, 
    dataTablehtml = '<center><div style="width:100%; align:center;"><table id="dataTable" class="display compact" style="margin-top: 30px; margin-bottom: 30px;">' +
        '<thead>' +
        '</thead>' +
        '<tbody>' +
        '</tbody>' +
      '</table></div></center>';

function invertColor(hexTripletColor) {
    var color = hexTripletColor;
    color = color.substring(1); // remove #
    color = parseInt(color, 16); // convert to integer
    color = 0xFFFFFF ^ color; // invert three bytes
    color = color.toString(16); // convert to hex
    color = ("000000" + color).slice(-6); // pad with leading zeros
    color = "#" + color; // prepend #
    return color;
}

function writeChart(submitted) {

  var $lineChart = $("#line-chart");

  var yscale = '';
  if ($("input[name='yscale']:checked").val()=='line') {
    yscale = 'linear';
  } else {
    yscale = 'logarithmic';
  }

  var data = {
    csrfmiddlewaretoken: csrftoken,
    groupby: groupby,
  }

  if (submitted) {
    data.region = $('#region').val();
    data.metric = $('#metric').val();
    if (($('#startDate').val()!='' && $('#endDate').val()!='')) {
      data.start_date = $('#startDate').val();
      data.end_date = $('#endDate').val();
    }
  }
  
  $.ajax({
    type: "POST",
    url: $lineChart.data("url"),
    data: data,
    success: function (data) {
      if (submitted==false) {
        if ($('#startDate').val()!=data.labels[0] || $('#endDate').val()!=data.labels[data.labels.length - 1]) {
          // let's initialize our datepicker
          $('#startDate').val(data.labels[0]);
          $('#endDate').val(data.labels[data.labels.length - 1]);
        }
      }
      var chart_data = [];
      for (var region in data.data) {
        if ($('#metric').val()=='positive_death_increase') {

          // positive increase
          var positive_values = [];
          var death_values = [];
          var positive_values_mavg7 = [];
          var death_values = [];
          var death_values_mavg7 = [];
          var max_left = 0;
          var max_left_label = '';
          var max_right = 0;
          var max_right_label = '';
          var get_scales = [];
          for (var x in data.data[region]['value']) {
            //console.log(x);
            positive_values.push(data.data[region]['value'][x][0]);
            death_values.push(data.data[region]['value'][x][1]);
            positive_values_mavg7.push(data.data[region]['mavg'][x][0]);
            death_values_mavg7.push(data.data[region]['mavg'][x][1]);
            if (data.data[region]['value'][x][0] > max_left) {
              max_left = data.data[region]['value'][x][0];
              max_left_label = region+' positives';
            }
            if (data.data[region]['value'][x][1] > max_right) {
              max_right = data.data[region]['value'][x][1];
              max_right_label = region+' deaths';
            }
          }
          chart_data.push({
            data: positive_values,
            label: region+' positives',
            yAxisID: max_left_label,
            borderColor: data.data[region]['border_color'],
            fill: false,
            borderWidth: 1,
          });
          // death increase
          chart_data.push({
            data: death_values,
            label: region+' deaths',
            yAxisID: max_right_label,
            borderColor: invertColor(data.data[region]['border_color']),
            fill: false,
            borderWidth: 1,
          });
          if ($("input[name='mavg']:checked").is(':checked')) {
            chart_data.push({
              data: positive_values_mavg7,
              label: region+' positives mavg7',
              yAxisID: max_left_label,
              borderColor: data.data[region]['border_color'],
              fill: false,
              borderDash: [3, 3],
              borderWidth: 1,
            });
            chart_data.push({
              data: death_values_mavg7,
              label: region+' deaths mavg7',
              yAxisID: max_right_label,
              borderColor: invertColor(data.data[region]['border_color']),
              fill: false,
              borderDash: [3, 3],
              borderWidth: 1,
            });
          }
          get_yscales = [{
            display: true,
            id: max_left_label,
            type: yscale,
            position: 'left',
            scaleLabel: {
              display: true,
              labelString: 'positives'
            }
          }, {
            display: true,
            id: max_right_label,
            type: yscale,
            position: 'right',
            scaleLabel: {
              display: true,
              labelString: 'deaths'
            }
          }];
        } else {
          chart_data.push({
            data: data.data[region]['value'],
            label: region,
            borderColor: data.data[region]['border_color'],
            fill: false,
            borderWidth: 1,
          });
          if ($("input[name='mavg']:checked").is(':checked')) {
            chart_data.push({
              data: data.data[region]['mavg'],
              label: region+' mavg7',
              borderColor: data.data[region]['border_color'],
              fill: false,
              borderDash: [5,5],
              borderWidth: 1,
            });
          }
          get_yscales = [{ display: true, type: yscale }];
        }
      }
      if (typeof myChart !== 'undefined') {
        myChart.destroy();
      }
      myChart = new Chart(document.getElementById("line-chart"), {
        type: 'line',
        data: {
          labels: data.labels,
          datasets: chart_data
        },
        options: {
          responsive: true,
          aspectRatio: 2,
          maintainAspectRatio: false,
          title: {
            display: true,
            text: data.metric_label
          },
          scales: {
            xAxes: [{
              display: true,
            }],
            yAxes: get_yscales
          },
        }
      });
    }
  });
}

function writeTable(rewrite) {

  $('#dataTable').empty();

  if ( $.fn.DataTable.isDataTable('#dataTable') ) {
    $('#dataTable').DataTable().destroy(true);
    $('#datatablecontainer').append(dataTablehtml);
  }

  if (groupby=='country') {
    targets = [1, 2, 3, 4, 5, 6, 7, 8, 9], //, 10, 11];
    column_titles = [
      { 'title': 'State' },
      { 'title': 'Positives Cumul.' },
      { 'title': 'Positives 24h' },
      { 'title': 'Deaths Cumul.' },
      { 'title': 'Deaths 24h' },
      { 'title': 'Fatality Rate' },
      { 'title': 'Tests Taken Cumul.' },
      { 'title': 'Hospitalization Cumul.' },
      { 'title': 'Deaths 7-Day Regr. Slope' },
      { 'title': 'Positives 7-Day Regr. Slope' },
      //{ 'title': 'Deaths 7-Day Trend' },
      //{ 'title': 'Positives 7-Day Trend' },
    ];
  } else {
    targets = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], //, 11, 12];
    column_titles = [
      { 'title': 'Country' },
      { 'title': 'Confirmed Cumul.' },
      { 'title': 'Confirmed 24h' },
      { 'title': 'Deaths Cumul.' },
      { 'title': 'Deaths 24h' },
      { 'title': 'Fatality Rate' },
      { 'title': 'Recovered Cumul.' },
      { 'title': 'Recovered 24h' },
      { 'title': 'Recovery Rate' },
      { 'title': 'Deaths 7-Day Regr. Slope' },
      { 'title': 'Confirmed 7-Day Regr. Slope' },
      //{ 'title': 'Deaths 7-Day Trend' },
      //{ 'title': 'Confirmed 7-Day Trend' },
    ];
  }

  myTable = $('#dataTable').DataTable({
    sScrollX: true,
    paging: false,
    bInfo: false,
    order: [[ 3, "desc" ]],
    columnDefs: [
    {
        targets: targets,
        className: 'dt-body-right'
    }
    ],
    columns: column_titles,
  });
  $.ajax({
    type: "POST",
    url: "/current_snapshot/",
    data: {
      csrfmiddlewaretoken: csrftoken,
      groupby: groupby,
    },
    success: function (data) {
      myTable.clear();
      var result;
      if (groupby=='country') {
        result = data.data.map(function(item){
          var result = [];
          result.push(item.state);
          result.push(item.positive);
          result.push(item.positive_increase);
          result.push(item.death);
          result.push(item.death_increase);
          result.push(item.death_rate);
          result.push(item.total_test_results);
          result.push(item.hopitalized);
          result.push(item.death_increase_regr_slope);
          result.push(item.positive_increase_regr_slope);
          //result.push(item.death_trend_7day);
          //result.push(item.positive_trend_7day);
          return result;
        });
      } else {
        result = data.data.map(function(item){
          var result = [];
          result.push(item.country);
          result.push(item.confirmed);
          result.push(item.confirmed_daily);
          result.push(item.deaths);
          result.push(item.deaths_daily);
          result.push(item.death_rate);
          result.push(item.recovered);
          result.push(item.recovered_daily);
          result.push(item.recovery_rate);
          result.push(item.deaths_daily_regr_slope);
          result.push(item.confirmed_daily_regr_slope);
          //result.push(item.deaths_trend_7day);
          //result.push(item.confirmed_trend_7day);
          return result;
        });
      }
      myTable.rows.add(result);
      myTable.draw();
    }
  });
}

function cleanUrl() {
  var uri = window.location.toString();
  if (uri.indexOf("?") > 0) {
    var clean_uri = uri.substring(0, uri.indexOf("?"));
    window.history.replaceState({}, document.title, clean_uri);
  }
  else if (uri.indexOf("#") > 0) {
    var clean_uri = uri.substring(0, uri.indexOf("#"));
    history.pushState('', document.title, window.location.pathname);
  }
}

$("#clear_region").click(function () {
  $('#region').val([]).trigger('change');
});

$("#select_region_filter_death_increase").click(function (x) {
  $('#region').val([]).trigger('change');
  $.ajax({
    type: "POST",
    url: '/regions_by/',
    data: {
      by: 'death_increase',
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      $('#region').val(data.regions).trigger('change');
      writeChart(true);
    }
  });
});

$("#select_region_filter_death").click(function (x) {
  $('#region').val([]).trigger('change');
  $.ajax({
    type: "POST",
    url: '/regions_by/',
    data: {
      by: 'death',
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      $('#region').val(data.regions).trigger('change');
      writeChart(true);
    }
  });
});

$("#select_region_filter_positive_increase").click(function (x) {
  $('#region').val([]).trigger('change');
  $.ajax({
    type: "POST",
    url: '/regions_by/',
    data: {
      by: 'positive_increase',
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      $('#region').val(data.regions).trigger('change');
      writeChart(true);
    }
  });
});

$("#select_region_filter_positive").click(function (x) {
  $('#region').val([]).trigger('change');
  $.ajax({
    type: "POST",
    url: '/regions_by/',
    data: {
      by: 'positive',
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      $('#region').val(data.regions).trigger('change');
      writeChart(true);
    }
  });
});

$("#select_region_filter_death_slope").click(function (x) {
  $('#region').val([]).trigger('change');
  $.ajax({
    type: "POST",
    url: '/regions_by/',
    data: {
      by: 'death_increase_regr_slope',
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      $('#region').val(data.regions).trigger('change');
      writeChart(true);
    }
  });
});

$("#select_region_filter_positive_slope").click(function (x) {
  $('#region').val([]).trigger('change');
  $.ajax({
    type: "POST",
    url: '/regions_by/',
    data: {
      by: 'positive_increase_regr_slope',
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      $('#region').val(data.regions).trigger('change');
      writeChart(true);
    }
  });
});

$("#filter_form").submit(function(e) {
  e.preventDefault();
  writeChart(true);
  return false;
});

function copyClipboard(copy_string) {
  document.getElementById('copy_to_clipboard').value = copy_string;
  var input = document.getElementById('copy_to_clipboard');
  input.focus();
  var userAgent = window.navigator.userAgent;
  if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
    input.setSelectionRange(0, this.value.length);
  }
  else {
    input.select();
  }
  return false;
}

$("#copy_link").click(function(e) {
  cleanUrl();
  var hostname = window.location.href;
  var copy_metric = $('#metric option:selected').val();
  var copy_yscale = $("input[name='yscale']:checked").val();
  var copy_mavg_checked = $("input[name='mavg']:checked").is(':checked') ? 1 :0;
  var copy_regions = $('#region').val().join('|');
  var copy_start_date = $('#startDate').val();
  var copy_end_date = $('#endDate').val();
  var copy_copied = 1;

  var redirect =
    '?metric='+copy_metric+
    '&yscale='+copy_yscale+
    '&mavg_checked='+copy_mavg_checked+
    '&regions='+copy_regions+
    '&start_date='+copy_start_date+
    '&end_date='+copy_end_date+
    '&groupby='+groupby+
    '&copied=1';

  $.ajax({
    type: "POST",
    url: '/url_shortcut/',
    data: {
      redirect: redirect,
      csrfmiddlewaretoken: csrftoken,
    },
    success: function (data) {
      console.log(data.redirect_hash);
      var hash = data.redirect_hash;
      var copy_string = hostname+'?rh='+hash;
      copyClipboard(copy_string);
      return false;
    }
  });
  return false;
});

function update_metric_menu(set_default_metrics) {
  $('#metric').empty();
  $.ajax({
    type: "POST",
    url: '/get_metric_config/',
    data: {
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    async: false,
    success: function (data) {
      for (var x in data.metrics) {
        if (set_default_metrics && set_default_metrics==x) {
          $('#metric').append('<option value="'+x+'" selected>'+data.metrics[x]+'</option>');
        } else {
          $('#metric').append('<option value="'+x+'">'+data.metrics[x]+'</option>');
        }
      }
    }
  });
}

function update_default_regions(set_default_regions) {
  $('#region').empty();
  $.ajax({
    type: "POST",
    url: '/get_default_regions/',
    data: {
      groupby: groupby,
      csrfmiddlewaretoken: csrftoken,
    },
    async: false,
    success: function (data) {
      for (var x in data.all_regions) {
        $('#region').append('<option value="'+x+'">'+data.all_regions[x]+'</option>');
      }
      if (set_default_regions && Array.isArray(set_default_regions)) {
        $('#region').val(set_default_regions).trigger('change');
      } else {
        $('#region').val(data.selected_regions).trigger('change');
      }
    }
  });
}

$("#select_main_view_by_us").click(function (x) {
  cleanUrl();
  $("#select_main_view_by_us").css("font-weight","bold");
  $("#select_main_view_by_world").css("font-weight","normal");
  groupby = 'country'
  update_metric_menu();
  update_default_regions();
  writeChart(false);
  writeTable(true);
});

$("#select_main_view_by_world").click(function (x) {
  cleanUrl();
  $("#select_main_view_by_us").css("font-weight","normal");
  $("#select_main_view_by_world").css("font-weight","bold");
  groupby = 'world'
  update_metric_menu();
  update_default_regions();
  writeChart(false);
  writeTable(true);
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

$("#apply").click(function (x) {
  var regions = $('#region').val();
  if ($('#metric').val() == 'positive_death_increase' && Object.keys(regions).length > 1) {
    alert('Please select only one region for Daily Positive and Deaths metric');
    return false;
  }
});

$(document).ready(function() {

  $(".js-example-placeholder-multiple").select2({});
  csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

  // if values coming in from GET, let's set them
  let searchParams = new URLSearchParams(window.location.search)
  if (searchParams.has('rh')) {

    $.ajax({
      type: "POST",
      url: '/get_url/',
      data: {
        rh: searchParams.get('rh'),
        csrfmiddlewaretoken: csrftoken,
      },
      async: false,
      success: function (data) {
        let rhParams = new URLSearchParams(data.redirect);
        groupby = rhParams.get('groupby');

        if (groupby=='country') {
          $("#select_main_view_by_us").css("font-weight","bold");
          $("#select_main_view_by_world").css("font-weight","normal");
        } else {
          $("#select_main_view_by_us").css("font-weight","normal");
          $("#select_main_view_by_world").css("font-weight","bold");
        }
        update_metric_menu(rhParams.get('metric'));
        $('input[name=yscale][value="'+rhParams.get('yscale')+'"]').prop('checked', true);
        if (rhParams.get('mavg_checked')=='1') {
          $('input[name=mavg]').prop('checked', true);
        }
        update_default_regions(rhParams.get('regions').split('|'));
        start_date = rhParams.get('start_date');
        end_date = rhParams.get('end_date');
        $("#startDate").datepicker({
            dateFormat: "yy-mm-dd",
          }).datepicker('setDate', start_date);
        $("#endDate").datepicker({
          dateFormat: "yy-mm-dd"
        }).datepicker('setDate', end_date);
        writeChart(true);
        cleanUrl();
      }
    });
  } else {
    // no params
    $("#select_main_view_by_us").css("font-weight","bold");
    update_metric_menu();
    update_default_regions();
    $("#startDate").datepicker({
      dateFormat: "yy-mm-dd"
    });
    $("#endDate").datepicker({
      dateFormat: "yy-mm-dd"
    });
    writeChart(false);
  }
  writeTable(false);
  return false;
});
