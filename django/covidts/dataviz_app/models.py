from django.db import models

class CovidStates(models.Model):
    date = models.DateField()
    state = models.CharField(max_length=5)
    positive = models.IntegerField(null=True)
    negative = models.IntegerField(null=True)
    pending = models.IntegerField(null=True)
    hospitalize_currently = models.IntegerField(null=True)
    hospitalize_cumulative = models.IntegerField(null=True)
    in_icu_currently = models.IntegerField(null=True)
    in_icu_cumulative = models.IntegerField(null=True)
    on_ventilator_currently = models.IntegerField(null=True)
    on_ventilator_cumulative = models.IntegerField(null=True)
    recovered = models.IntegerField(null=True)
    hash_id = models.CharField(max_length=40, null=True)
    death = models.IntegerField(null=True)
    hopitalized = models.IntegerField(null=True)
    total = models.IntegerField(null=True)
    total_test_results = models.IntegerField(null=True)
    pos_neg = models.IntegerField(null=True)
    fips = models.IntegerField(null=True)
    death_increase = models.IntegerField(null=True)
    hospitalized_increase = models.IntegerField(null=True)
    negative_increase = models.IntegerField(null=True)
    positive_increase = models.IntegerField(null=True)
    total_test_results_increase = models.IntegerField(null=True)
    death_increase_regr_slope = models.FloatField(null=True)
    positive_increase_regr_slope = models.FloatField(null=True)
    death_increase_regr_intercept = models.FloatField(null=True) 
    positive_increase_regr_intercept = models.FloatField(null=True)
    death_increase_regr_stderr = models.FloatField(null=True) 
    positive_increase_regr_stderr = models.FloatField(null=True)
    death_increase_mavg7 = models.FloatField(null=True)
    positive_increase_mavg7 = models.FloatField(null=True)

    @property
    def positive_test_rate(self):
        if self.total_test_results > 0 and self.death is not None:
            return round(self.positive / self.total_test_results, 3)
        else:
            return None
    
    @property
    def death_rate(self):
        if self.positive > 0 and self.death is not None:
            return round(self.death / self.positive, 3)
        else:
            return None

    @property
    def hospitalization_rate(self):
        if self.positive > 0 and self.hospitalize_currently is not None:
            return round(self.hospitalize_currently / self.positive, 3)
        else:
            return None

    @property
    def in_icu_rate(self):
        if self.positive > 0 and self.in_icu_currently is not None:
            return round(self.in_icu_currently / self.positive, 3)
        else:
            return None

    @property
    def on_ventilator_rate(self):
        if self.positive > 0 and self.on_ventilator_currently is not None:
            return round(self.on_ventilator_currently / self.positive, 3)
        else:
            return None

    @property
    def region(self):
        return self.state

    @property
    def death_trend_7day(self):
        if (self.death_increase_mavg7 or 0)==0:
            return None
        elif self.death_increase_mavg7 < 0:
            return 0
        else:
            return round(self.death_increase_regr_slope / self.death_increase_mavg7, 3)

    @property
    def positive_trend_7day(self):
        if (self.positive_increase_mavg7 or 0)==0:
            return None
        elif self.positive_increase_mavg7 < 0:
            return 0
        else:
            return round(self.positive_increase_regr_slope / self.positive_increase_mavg7, 3)

    @property
    def positive_death_increase(self):
        return (self.positive_increase, self.death_increase)

    @property
    def positive_death_increase_7day(self):
        return (self.positive_trend_7day, self.death_trend_7day)

    def __str__(self):
        return '%s %s' % (self.state, self.date)


class CovidCountries(models.Model):
    date = models.DateField()
    country = models.CharField(max_length=40)
    confirmed = models.IntegerField(null=True)
    deaths = models.IntegerField(null=True)
    recovered = models.IntegerField(null=True)
    confirmed_daily = models.IntegerField(null=True)
    deaths_daily = models.IntegerField(null=True)
    recovered_daily = models.IntegerField(null=True)
    deaths_daily_regr_slope = models.FloatField(null=True)
    deaths_daily_regr_intercept = models.FloatField(null=True)
    deaths_daily_regr_stderr = models.FloatField(null=True)
    confirmed_daily_regr_slope = models.FloatField(null=True)
    confirmed_daily_regr_intercept = models.FloatField(null=True)
    confirmed_daily_regr_stderr = models.FloatField(null=True)
    deaths_daily_mavg7 = models.FloatField(null=True)
    confirmed_daily_mavg7 = models.FloatField(null=True)

    @property
    def death_rate(self):
        if self.confirmed > 0 and self.deaths is not None:
            return round(self.deaths / self.confirmed, 3)
        else:
            return None

    @property
    def recovery_rate(self):
        if self.confirmed > 0 and self.recovered is not None:
            return round(self.recovered / self.confirmed, 3)
        else:
            return None

    @property
    def region(self):
        return self.country

    @property
    def deaths_trend_7day(self):
        if (self.deaths_daily_mavg7 or 0)==0:
            return None
        elif self.deaths_daily_mavg7 < 0:
            return 0
        else:
            return round(self.deaths_daily_regr_slope / self.deaths_daily_mavg7, 3)

    @property
    def confirmed_trend_7day(self):
        if (self.confirmed_daily_mavg7 or 0)==0:
            return None
        elif self.confirmed_daily_mavg7 < 0:
            return 0
        else:
            return round(self.confirmed_daily_regr_slope / self.confirmed_daily_mavg7, 3)

    @property
    def positive_death_increase(self):
        return (self.confirmed_daily, self.deaths_daily)

    @property
    def positive_death_increase_7day(self):
        return (self.confirmed_trend_7day, self.deaths_trend_7day)

    def __str__(self):
        return '%s %s' % (self.country, self.date)


class RedirectHash(models.Model):
    redirect_hash = models.CharField(max_length=64)
    redirect = models.TextField(null=True)