from .constants import redirect_hash_characters, country_metadata, world_metadata, dark_colors
from random import randrange, choice
import numpy as np
import pprint
from matplotlib.colors import hsv_to_rgb, to_hex

def ten_digit_hash():
    get_hash = list()
    while len(get_hash) <= 10:
        get_hash.append(redirect_hash_characters[randrange(0, len(redirect_hash_characters))])

    return ''.join(get_hash)


def rand_col(min_v=0.2, max_v=1.0):
    return choice(dark_colors)
    #hsv = np.concatenate([np.random.rand(2), np.random.uniform(min_v, max_v, size=1)])
    #return to_hex(hsv_to_rgb(hsv))

def replace_country_colors():
    colors = set()
    random_color = ''
    new_country_metadata = dict()
    for country in country_metadata:
        while 1:
            random_color = rand_col()
            if random_color not in colors:
                colors.add(random_color)
                new_country_metadata[country] = {
                    'display_region': country_metadata[country]['display_region'],
                    'html_color': random_color
                }
                break

    pprint.pprint(new_country_metadata)

def replace_world_colors():
    colors = set()
    random_color = ''
    new_world_metadata = dict()
    for country in world_metadata:
        while 1:
            random_color = rand_col()
            if random_color not in colors:
                colors.add(random_color)
                new_world_metadata[country] = {
                    'display_region': world_metadata[country]['display_region'],
                    'html_color': random_color
                }
                break

    pprint.pprint(new_world_metadata)