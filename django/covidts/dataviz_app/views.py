from django.shortcuts import render
from django.db.models import Sum
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from datetime import datetime
from .constants import country_metadata, world_metadata, country_metric_config, world_metric_config, country_to_world_metric_mapping
from .models import CovidStates, CovidCountries, RedirectHash
from .queries import top5_states, top5_countries, top5_regions
from .utils import ten_digit_hash
import pprint

# Create your views here.
def home(request):
    # we need this data to prepopulate filters
    return render(request, 'home.html', {
        'metric_config': country_metric_config,
        'selected_regions': top5_states(),
        'region_metadata': country_metadata,
    })

@require_http_methods(["POST"])
def get_metric_config(request):
    groupby = request.POST.get('groupby', 'country')
    if groupby=='country':
        return JsonResponse(data={
            'metrics': country_metric_config,
        })
    else:
        return JsonResponse(data={
            'metrics': world_metric_config,
        })

@require_http_methods(["POST"])
def get_default_regions(request):
    groupby = request.POST.get('groupby', 'country')
    all_regions = {}
    region_metadata = country_metadata if groupby=='country' else world_metadata
    for region in region_metadata:
        all_regions[region] = region_metadata[region]['display_region']
    return JsonResponse(data={
        'selected_regions': top5_regions(groupby),
        'all_regions': all_regions,
    })

@require_http_methods(["POST"])
def line_chart(request):
    selected_regions = request.POST.getlist('region[]', [])
    groupby = request.POST.get('groupby', 'country')
    start_date = request.POST.get('start_date', None)
    end_date = request.POST.get('end_date', None)

    
    if start_date is not None:
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
    else:
        start_date = datetime.strptime('2020-10-01', '%Y-%m-%d')

    if end_date is not None:
        end_date = datetime.strptime(end_date, '%Y-%m-%d')

    metric = ''
    labels = set()
    get_data = {}
    if len(selected_regions)==0:
        if groupby=='country':
            selected_regions = top5_states()
        else:
            selected_regions = top5_countries()

    if groupby=='country':
        if start_date is not None and end_date is not None:
            queryset = CovidStates.objects.filter(state__in=selected_regions, date__range=[start_date, end_date]).order_by('date')
        elif start_date is not None and end_date is None:
            queryset = CovidStates.objects.filter(state__in=selected_regions, date__gte=start_date).order_by('date')
        else:
            queryset = CovidStates.objects.filter(state__in=selected_regions).order_by('date')
        region_metadata = country_metadata
        metric = request.POST.get('metric', 'death_increase')
        metric_config = country_metric_config
    else:
        if start_date is not None and end_date is not None:
            queryset = CovidCountries.objects.filter(country__in=selected_regions, date__range=[start_date, end_date]).order_by('date')
        elif start_date is not None and end_date is None:
            queryset = CovidCountries.objects.filter(country__in=selected_regions, date__gte=start_date).order_by('date')
        else:
            queryset = CovidCountries.objects.filter(country__in=selected_regions).order_by('date')
        region_metadata = world_metadata
        metric = request.POST.get('metric', 'deaths_daily')
        metric_config = world_metric_config

    for entry in queryset:
        labels.add(entry.date.strftime("%Y-%m-%d"))

    labels = sorted(list(labels))
    for entry in queryset:
        entry_date = entry.date.strftime("%Y-%m-%d")

        get_metric_value = getattr(entry, metric)

        if metric == 'positive_death_increase':
            if entry.region not in get_data:
                # let's create all labels for each state
                get_data[entry.region] = {
                    'value': [(None, None)] * len(labels),
                    'mavg': [(None, None)] * len(labels),
                    'border_color': region_metadata[entry.region]['html_color'],
                }

            if get_metric_value is None:
                get_data[entry.region]['value'][labels.index(entry_date)] = (0, 0)
            elif get_metric_value[0] is None and get_metric_value[1] is not None:
                get_data[entry.region]['value'][labels.index(entry_date)] = (0, get_metric_value[1])
            elif get_metric_value[0] is not None and get_metric_value[1] is None:
                get_data[entry.region]['value'][labels.index(entry_date)] = (get_metric_value[0], 0)
            elif type(get_metric_value) is tuple:
                get_data[entry.region]['value'][labels.index(entry_date)] = get_metric_value

             # start index for moving average, can't go below 0
            start_index = 0 if ((labels.index(entry_date)-7) < 0) else (labels.index(entry_date)-7)
            mavg_sum_0 = sum([x[0] or 0 for x in get_data[entry.region]['value'][start_index:labels.index(entry_date)]])
            mavg_sum_1 = sum([x[1] or 0 for x in get_data[entry.region]['value'][start_index:labels.index(entry_date)]])
            mavg_length = len(get_data[entry.region]['value'][start_index:labels.index(entry_date)])
            get_data[entry.region]['mavg'][labels.index(entry_date)] = (None, None) if mavg_length==0 else (round(mavg_sum_0 / mavg_length, 3), round(mavg_sum_1 / mavg_length, 3))

        else:
            if entry.region not in get_data:
                # let's create all labels for each state
                get_data[entry.region] = {
                    'value': [None] * len(labels),
                    'mavg': [None] * len(labels),
                    'border_color': region_metadata[entry.region]['html_color'],
                }

            if get_metric_value is None or get_metric_value < 0:
                get_data[entry.region]['value'][labels.index(entry_date)] = 0
            else:
                get_data[entry.region]['value'][labels.index(entry_date)] = get_metric_value
            
            # start index for moving average, can't go below 0
            start_index = 0 if ((labels.index(entry_date)-7) < 0) else (labels.index(entry_date)-7)
            mavg_sum = sum([x or 0 for x in get_data[entry.region]['value'][start_index:labels.index(entry_date)]])
            mavg_length = len(get_data[entry.region]['value'][start_index:labels.index(entry_date)])
            get_data[entry.region]['mavg'][labels.index(entry_date)] = None if mavg_length==0 else round(mavg_sum / mavg_length, 3)

    return JsonResponse(data={
        'metric_label': metric_config[metric],
        'labels': sorted(list(labels)),
        'data': get_data
    })

@require_http_methods(["POST"])
def regions_by(request):
    metric = request.POST.get('by', 'positive')
    groupby = request.POST.get('groupby', 'country')
    n = request.POST.get('n', 5)

    # this is a bit hacky of a metric mapping
    if groupby=='country':
        max_date = CovidStates.objects.latest('date')
        queryset = CovidStates.objects.filter(date=max_date.date).order_by('-'+metric)[:n]
    else:
        metric = country_to_world_metric_mapping[metric]
        max_date = CovidCountries.objects.latest('date')
        queryset = CovidCountries.objects.filter(date=max_date.date).order_by('-'+metric)[:n]

    regions = []
    for entry in queryset:
        regions.append(entry.region)

    return JsonResponse(data={
        'regions': regions
        })

@require_http_methods(["POST"])
def current_snapshot(request):
    groupby = request.POST.get('groupby', 'country')
    if groupby=='country':
        max_date = CovidStates.objects.latest('date')
        queryset = CovidStates.objects.filter(date=max_date.date)
        get_data = []
        for entry in queryset:
            get_data.append({
                'state': entry.state,
                'positive': entry.positive,
                'positive_increase': entry.positive_increase,
                'death': entry.death,
                'death_increase': entry.death_increase,
                'death_rate': entry.death_rate,
                'total_test_results': entry.total_test_results,
                'hopitalized': entry.hopitalized,
                #'death_trend_7day': entry.death_trend_7day,
                #'positive_trend_7day': entry.positive_trend_7day,
                'death_increase_regr_slope': round(entry.death_increase_regr_slope, 3),
                'positive_increase_regr_slope': round(entry.positive_increase_regr_slope, 3),
            })
    else:
        max_date = CovidCountries.objects.latest('date')
        queryset = CovidCountries.objects.filter(date=max_date.date)
        get_data = []
        for entry in queryset:
            get_data.append({
                'country': entry.country,
                'confirmed': entry.confirmed,
                'confirmed_daily': entry.confirmed_daily,
                'deaths': entry.deaths,
                'deaths_daily': entry.deaths_daily,
                'death_rate': entry.death_rate,
                'recovered': entry.recovered,
                'recovered_daily': entry.recovered_daily,
                'recovery_rate': entry.recovery_rate,
                #'deaths_trend_7day': entry.deaths_trend_7day,
                #'confirmed_trend_7day': entry.confirmed_trend_7day,
                'deaths_daily_regr_slope': round(entry.deaths_daily_regr_slope, 3),
                'confirmed_daily_regr_slope': round(entry.confirmed_daily_regr_slope, 3),
            })
    return JsonResponse(data={
        'data': get_data,
        })

@require_http_methods(["POST"])
def url_shortcut(request):
    redirect = request.POST.get('redirect', None)

    if redirect is None:
        return False

    redirect_hash = ''
    get_redirect = RedirectHash.objects.filter(redirect=redirect)

    if len(get_redirect)==0:
        while 1:
            redirect_hash = ten_digit_hash()
            get_redirect = RedirectHash.objects.filter(redirect_hash=redirect_hash)
            if len(get_redirect)==0:
                set_redirect = RedirectHash.objects.create(redirect_hash=redirect_hash, redirect=redirect)
                break
    else:
        redirect_hash = get_redirect[0].redirect_hash

    return JsonResponse(data={
        'redirect_hash': redirect_hash,
    })

@require_http_methods(["POST"])
def get_url(request):
    redirect_hash = request.POST.get('rh', None)

    if redirect_hash is None:
        return False
    
    get_redirect = RedirectHash.objects.filter(redirect_hash=redirect_hash)

    if len(get_redirect) >0:
        return JsonResponse(data={
            'redirect': get_redirect[0].redirect,
        })
    else:
        return JsonResponse(data={
            'redirect': '',
        })
