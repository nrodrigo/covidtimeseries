from .models import CovidStates, CovidCountries
from .constants import country_metadata, world_metadata
from datetime import datetime, timedelta
from scipy import stats
import pandas as pd
import pprint

def top5_states():
    max_date = CovidStates.objects.latest('date')
    return list(CovidStates.objects.filter(date=max_date.date).order_by('-death')[:5].values_list('state', flat=True))

def top5_countries():
    max_date = CovidCountries.objects.latest('date')
    return list(CovidCountries.objects.filter(date=max_date.date).order_by('-deaths')[:5].values_list('country', flat=True))

def top5_regions(groupby):
    get_data = {}
    if groupby=='country':
        max_date = CovidStates.objects.latest('date')
        res = CovidStates.objects.filter(date=max_date.date).order_by('-death')[:5].values_list('state', flat=True)
    else:
        max_date = CovidCountries.objects.latest('date')
        res = CovidCountries.objects.filter(date=max_date.date).order_by('-deaths')[:5].values_list('country', flat=True)
    return list(res)

def states_7day_metrics_30days():
    max_date = CovidStates.objects.latest('date')
    datelist = list(map(lambda x: datetime.strftime(x, "%Y-%m-%d"), pd.date_range(start=max_date.date - timedelta(days=30), end=max_date.date).tolist()))

    get_data = {}
    queryset = CovidStates.objects.filter(date__range=[max_date.date - timedelta(days=30), max_date.date])
    for row in queryset:
        current_date = datetime.strftime(row.date, '%Y-%m-%d')
        if row.state not in get_data:
            get_data[row.state] = {
                'deaths': [0] * len(datelist),
                'positives': [0] * len(datelist),
                'object': [None] * len(datelist),
                'death_slope': [None] * len(datelist),
            }
        get_data[row.state]['deaths'][datelist.index(current_date)] =  0 if (row.death_increase or 0) < 0 else (row.death_increase or 0)
        get_data[row.state]['positives'][datelist.index(current_date)] = 0 if (row.positive_increase or 0) < 0 else (row.positive_increase or 0)
        get_data[row.state]['death_slope'][datelist.index(current_date)] = row.death_increase_regr_slope
        get_data[row.state]['object'][datelist.index(current_date)] = row

    for i in range(len(datelist)):
        # need at least 2 points for linear regression
        if i<6:
            continue

        if i > 6:
            min_i = i - 6
            max_i = i
        else:
            min_i = 0
            max_i = i

        x_axis = list(range(max_i - min_i +1))
        cur_date = datelist[i]

        for region in get_data:
            #print("%s %s %s" % (region, datelist[min_i], datelist[max_i]))
            if get_data[region]['object'][i] is not None and get_data[region]['death_slope'][i] is None:
                lr_deaths = stats.linregress(x_axis, get_data[region]['deaths'][min_i:max_i+1])
                lr_positives = stats.linregress(x_axis, get_data[region]['positives'][min_i:max_i+1])
                get_data[region]['object'][i].death_increase_mavg7 = sum(get_data[region]['deaths'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].positive_increase_mavg7 = sum(get_data[region]['positives'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].death_increase_regr_slope = lr_deaths.slope
                get_data[region]['object'][i].death_increase_regr_intercept = lr_deaths.intercept
                get_data[region]['object'][i].death_increase_regr_stderr = lr_deaths.stderr
                get_data[region]['object'][i].positive_increase_regr_slope = lr_positives.slope
                get_data[region]['object'][i].positive_increase_regr_intercept = lr_positives.intercept
                get_data[region]['object'][i].positive_increase_regr_stderr = lr_positives.stderr
                get_data[region]['object'][i].save()

def countries_7day_metrics_30days():
    max_date = CovidCountries.objects.latest('date')
    datelist = list(map(lambda x: datetime.strftime(x, "%Y-%m-%d"), pd.date_range(start=max_date.date - timedelta(days=30), end=max_date.date).tolist()))

    get_data = {}
    queryset = CovidCountries.objects.filter(date__range=[max_date.date - timedelta(days=30), max_date.date])
    for row in queryset:
        current_date = datetime.strftime(row.date, '%Y-%m-%d')
        if row.country not in get_data:
            get_data[row.country] = {
                'deaths': [0] * len(datelist),
                'positives': [0] * len(datelist),
                'object': [None] * len(datelist),
                'death_slope': [None] * len(datelist),
            }
        get_data[row.country]['deaths'][datelist.index(current_date)] =  0 if (row.deaths_daily or 0) < 0 else (row.deaths_daily or 0)
        get_data[row.country]['positives'][datelist.index(current_date)] = 0 if (row.confirmed_daily or 0) < 0 else (row.confirmed_daily or 0)
        get_data[row.country]['death_slope'][datelist.index(current_date)] = row.deaths_daily_regr_slope
        get_data[row.country]['object'][datelist.index(current_date)] = row

    for i in range(len(datelist)):
        # need at least 2 points for linear regression
        if i<6:
            continue

        if i > 6:
            min_i = i - 6
            max_i = i
        else:
            min_i = 0
            max_i = i

        x_axis = list(range(max_i - min_i +1))
        cur_date = datelist[i]

        for region in get_data:
            #print("%s %s %s" % (region, datelist[min_i], datelist[max_i]))
            if get_data[region]['object'][i] is not None and get_data[region]['death_slope'][i] is None:
                lr_deaths = stats.linregress(x_axis, get_data[region]['deaths'][min_i:max_i+1])
                lr_positives = stats.linregress(x_axis, get_data[region]['positives'][min_i:max_i+1])
                get_data[region]['object'][i].deaths_daily_mavg7 = sum(get_data[region]['deaths'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].confirmed_daily_mavg7 = sum(get_data[region]['positives'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].deaths_daily_regr_slope = lr_deaths.slope
                get_data[region]['object'][i].deaths_daily_regr_intercept = lr_deaths.intercept
                get_data[region]['object'][i].deaths_daily_regr_stderr = lr_deaths.stderr
                get_data[region]['object'][i].confirmed_daily_regr_slope = lr_positives.slope
                get_data[region]['object'][i].confirmed_daily_regr_intercept = lr_positives.intercept
                get_data[region]['object'][i].confirmed_daily_regr_stderr = lr_positives.stderr
                get_data[region]['object'][i].save()

def countries_7day_metrics_init():
    min_date = CovidCountries.objects.earliest('date')
    max_date = CovidCountries.objects.latest('date')
    datelist = list(map(lambda x: datetime.strftime(x, "%Y-%m-%d"), pd.date_range(start=min_date.date, end=max_date.date).tolist()))

    get_data = {}
    queryset = CovidCountries.objects.all()
    for row in queryset:
        current_date = datetime.strftime(row.date, '%Y-%m-%d')
        if row.country not in get_data:
            get_data[row.country] = {
                'deaths': [0] * len(datelist),
                'positives': [0] * len(datelist),
                'object': [None] * len(datelist),
            }
        get_data[row.country]['deaths'][datelist.index(current_date)] = 0 if (row.deaths_daily or 0) < 0 else (row.deaths_daily or 0)
        get_data[row.country]['positives'][datelist.index(current_date)] = 0 if (row.confirmed_daily or 0)< 0 else (row.confirmed_daily or 0)
        get_data[row.country]['object'][datelist.index(current_date)] = row

    for i in range(len(datelist)):
        # need at least 2 points for linear regression
        if i==0:
            continue

        if i > 6:
            min_i = i - 6
            max_i = i
        else:
            min_i = 0
            max_i = i

        x_axis = list(range(max_i - min_i +1))
        cur_date = datelist[i]

        for region in get_data:
            #print("%s %s %s" % (region, datelist[min_i], datelist[max_i]))
            if get_data[region]['object'][i] is not None:
                lr_deaths = stats.linregress(x_axis, get_data[region]['deaths'][min_i:max_i+1])
                lr_positives = stats.linregress(x_axis, get_data[region]['positives'][min_i:max_i+1])
                get_data[region]['object'][i].deaths_daily_mavg7 = sum(get_data[region]['deaths'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].confirmed_daily_mavg7 = sum(get_data[region]['positives'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].deaths_daily_regr_slope = lr_deaths.slope
                get_data[region]['object'][i].linear_regression_states_30dayslinear_regression_states_30days = lr_deaths.intercept
                get_data[region]['object'][i].deaths_daily_regr_stderr = lr_deaths.stderr
                get_data[region]['object'][i].confirmed_daily_regr_slope = lr_positives.slope
                get_data[region]['object'][i].confirmed_daily_regr_intercept = lr_positives.intercept
                get_data[region]['object'][i].confirmed_daily_regr_stderr = lr_positives.stderr
                get_data[region]['object'][i].save()

def states_7day_metrics_init():
    min_date = CovidStates.objects.earliest('date')
    max_date = CovidStates.objects.latest('date')
    datelist = list(map(lambda x: datetime.strftime(x, "%Y-%m-%d"), pd.date_range(start=min_date.date, end=max_date.date).tolist()))

    get_data = {}
    queryset = CovidStates.objects.all()
    for row in queryset:
        current_date = datetime.strftime(row.date, '%Y-%m-%d')
        if row.state not in get_data:
            get_data[row.state] = {
                'deaths': [0] * len(datelist),
                'positives': [0] * len(datelist),
                'object': [None] * len(datelist),
            }
        get_data[row.state]['deaths'][datelist.index(current_date)] =  0 if (row.death_increase or 0) < 0 else (row.death_increase or 0)
        get_data[row.state]['positives'][datelist.index(current_date)] = 0 if (row.positive_increase or 0) < 0 else (row.positive_increase or 0)
        get_data[row.state]['object'][datelist.index(current_date)] = row

    for i in range(len(datelist)):
        # need at least 2 points for linear regression
        if i==0:
            continue

        if i > 6:
            min_i = i - 6
            max_i = i
        else:
            min_i = 0
            max_i = i

        x_axis = list(range(max_i - min_i +1))
        cur_date = datelist[i]

        for region in get_data:
            #print("%s %s %s" % (region, datelist[min_i], datelist[max_i]))
            if get_data[region]['object'][i] is not None:
                lr_deaths = stats.linregress(x_axis, get_data[region]['deaths'][min_i:max_i+1])
                lr_positives = stats.linregress(x_axis, get_data[region]['positives'][min_i:max_i+1])
                get_data[region]['object'][i].death_increase_mavg7 = sum(get_data[region]['deaths'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].positive_increase_mavg7 = sum(get_data[region]['positives'][min_i:max_i+1]) / len(x_axis)
                get_data[region]['object'][i].death_increase_regr_slope = lr_deaths.slope
                get_data[region]['object'][i].death_increase_regr_intercept = lr_deaths.intercept
                get_data[region]['object'][i].death_increase_regr_stderr = lr_deaths.stderr
                get_data[region]['object'][i].positive_increase_regr_slope = lr_positives.slope
                get_data[region]['object'][i].positive_increase_regr_intercept = lr_positives.intercept
                get_data[region]['object'][i].positive_increase_regr_stderr = lr_positives.stderr
                get_data[region]['object'][i].save()