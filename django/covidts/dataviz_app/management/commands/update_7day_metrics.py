from django.core.management.base import BaseCommand
from dataviz_app.queries import states_7day_metrics_init, countries_7day_metrics_init, states_7day_metrics_30days, countries_7day_metrics_30days

class Command(BaseCommand):
    def handle(self, **options):
        #states_7day_metrics_init()
        #countries_7day_metrics_init()
        states_7day_metrics_30days()
        countries_7day_metrics_30days()