from django.core.management.base import BaseCommand
from dataviz_app.covid_data import insert_covid_data_countries, insert_covid_data_states
from dataviz_app.queries import countries_7day_metrics_init, states_7day_metrics_init

class Command(BaseCommand):
    def handle(self, **options):
        insert_covid_data_states()
        insert_covid_data_countries()
        states_7day_metrics_init()
        countries_7day_metrics_init()