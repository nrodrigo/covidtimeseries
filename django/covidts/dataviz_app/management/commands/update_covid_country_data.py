from django.core.management.base import BaseCommand
from dataviz_app.covid_data import insert_covid_data_countries

class Command(BaseCommand):
    def handle(self, **options):
        insert_covid_data_countries()