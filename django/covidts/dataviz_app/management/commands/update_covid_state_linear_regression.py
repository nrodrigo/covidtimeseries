from django.core.management.base import BaseCommand
from dataviz_app.queries import linear_regression_states

class Command(BaseCommand):
    def handle(self, **options):
        linear_regression_states()