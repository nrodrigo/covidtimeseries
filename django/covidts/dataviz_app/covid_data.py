from covidts.settings import COVID_DATA_ROOT
from datetime import datetime, timedelta
from django.db import connection
import json
import os
import pprint
import urllib.request
from .models import CovidStates, CovidCountries

"""
from dataviz_app import covid_data
covid_data.insert_covid_data_states()
"""
def insert_covid_data_states():
    url = 'https://covidtracking.com/api/v1/states/daily.json'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())

    for row in data:
        get_date = str(row['date'])[0:4]+'-'+str(row['date'])[4:6]+'-'+str(row['date'])[6:8]
        a = CovidStates.objects.get_or_create(
            date=datetime.strptime(get_date, '%Y-%m-%d'),
            state=row['state'],
            defaults={
                'positive': row.get('positive', None),
                'negative': row.get('negative', None),
                'pending': row.get('pending', None),
                'hospitalize_currently': row.get('hospitalizedCurrently', None),
                'hospitalize_cumulative': row.get('hospitalizedCumulative', None),
                'in_icu_currently': row.get('inIcuCurrently', None),
                'in_icu_cumulative': row.get('inIcuCumulative', None),
                'on_ventilator_currently': row.get('onVentilatorCurrently', None),
                'on_ventilator_cumulative': row.get('onVentilatorCumulative', None),
                'recovered': row.get('recovered', None),
                'hash_id': row.get('hash', None),
                'death': row.get('death', None),
                'hopitalized': row.get('hospitalized', None),
                'total': row.get('total', None),
                'total_test_results': row.get('totalTestResults', None),
                'pos_neg': row.get('posNeg', None),
                'fips': row.get('fips', None),
                'death_increase': row.get('deathIncrease', None),
                'hospitalized_increase': row.get('hospitalizedIncrease', None),
                'negative_increase': row.get('negativeIncrease', None),
                'positive_increase': row.get('positiveIncrease', None),
                'total_test_results_increase': row.get('totalTestResultsIncrease', None),
            }
        )
        if not a[1]:
            a[0].positive = row.get('positive', None)
            a[0].negative = row.get('negative', None)
            a[0].pending = row.get('pending', None)
            a[0].hospitalize_currently = row.get('hospitalizedCurrently', None)
            a[0].hospitalize_cumulative = row.get('hospitalizedCumulative', None)
            a[0].in_icu_currently = row.get('inIcuCurrently', None)
            a[0].in_icu_cumulative = row.get('inIcuCumulative', None)
            a[0].on_ventilator_currently = row.get('onVentilatorCurrently', None)
            a[0].on_ventilator_cumulative = row.get('onVentilatorCumulative', None)
            a[0].recovered = row.get('recovered', None)
            a[0].hash_id = row.get('hash', None)
            a[0].death = row.get('death', None)
            a[0].hopitalized = row.get('hospitalized', None)
            a[0].total = row.get('total', None)
            a[0].total_test_results = row.get('totalTestResults', None)
            a[0].pos_neg = row.get('posNeg', None)
            a[0].fips = row.get('fips', None)
            a[0].death_increase = row.get('deathIncrease', None)
            a[0].hospitalized_increase = row.get('hospitalizedIncrease', None)
            a[0].negative_increase = row.get('negativeIncrease', None)
            a[0].positive_increase = row.get('positiveIncrease', None)
            a[0].total_test_results_increase = row.get('totalTestResultsIncrease', None)
            a[0].save()

    return True

"""
from dataviz_app import covid_data
covid_data.insert_covid_data_countries()
"""
def insert_covid_data_countries():
    url = 'https://pomber.github.io/covid19/timeseries.json'
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())

    datelist = set()
    get_data = {}

    for country in data:
        for row in data[country]:
            (y, m, d) = row['date'].split('-')
            get_date = "%s-%s-%s" % (str(y), str(m).zfill(2), str(d).zfill(2))
            datelist.add(get_date)

    datelist = sorted(list(datelist))

    for country in data:
        for row in data[country]:
            (y, m, d) = row['date'].split('-')
            get_date = "%s-%s-%s" % (str(y), str(m).zfill(2), str(d).zfill(2))
            if country not in get_data:
                get_data[country] = {
                    'deaths': [None] * len(datelist),
                    'confirmed': [None] * len(datelist),
                    'recovered': [None] * len(datelist),
                    'object': [None] * len(datelist),
                }
            a = CovidCountries.objects.get_or_create(
                date=datetime.strptime(get_date, '%Y-%m-%d'),
                country=country,
                defaults={
                    'confirmed': row.get('confirmed', None),
                    'deaths': row.get('deaths', None),
                    'recovered': row.get('recovered', None),
                    'confirmed_daily': 0,
                    'deaths_daily': 0,
                    'recovered_daily': 0,
                }
            )
            if not a[1]: # if the row exists, update info
                a[0].confirmed = row.get('confirmed', None)
                a[0].deaths = row.get('deaths', None)
                a[0].recovered = row.get('recovered', None)
                a[0].save()
            get_data[country]['deaths'][datelist.index(get_date)] = row.get('deaths', None)
            get_data[country]['confirmed'][datelist.index(get_date)] = row.get('confirmed', None)
            get_data[country]['recovered'][datelist.index(get_date)] = row.get('recovered', None)
            get_data[country]['object'][datelist.index(get_date)] = a[0]

    # need to update data in case it changes historically
    for d in datelist:
        # skip if first day, won't have a prev day to compare
        if datelist.index(d)==0:
            continue
        for country in get_data:
            if get_data[country]['object'][datelist.index(d)] is not None:
                get_data[country]['object'][datelist.index(d)].deaths_daily = (get_data[country]['deaths'][datelist.index(d)] or 0) - (get_data[country]['deaths'][datelist.index(d)-1] or 0)
                get_data[country]['object'][datelist.index(d)].confirmed_daily = (get_data[country]['confirmed'][datelist.index(d)] or 0) - (get_data[country]['confirmed'][datelist.index(d)-1] or 0)
                get_data[country]['object'][datelist.index(d)].recovered_daily = (get_data[country]['recovered'][datelist.index(d)] or 0) - (get_data[country]['recovered'][datelist.index(d)-1] or 0)
                get_data[country]['object'][datelist.index(d)].save()

    return True