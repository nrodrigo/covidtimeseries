from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('line-chart/', views.line_chart, name='line-chart'),
    path('regions_by/', views.regions_by, name='regions_by'),
    path('current_snapshot/', views.current_snapshot, name='current_snapshot'),
    path('get_metric_config/', views.get_metric_config, name='get_metric_config'),
    path('get_default_regions/', views.get_default_regions, name='get_default_regions'),
    path('url_shortcut/', views.url_shortcut, name='url_shortcut'),
    path('get_url/', views.get_url, name='get_url'),
]