from django.apps import AppConfig


class DatavizAppConfig(AppConfig):
    name = 'dataviz_app'
